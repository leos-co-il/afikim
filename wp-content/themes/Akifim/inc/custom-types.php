<?php
if(CATALOG){
	function product_post_type() {

		$labels = array(
			'name'                => 'רכבים',
			'singular_name'       => 'רכב',
			'menu_name'           => 'רכבים',
			'parent_item_colon'   => 'פריט אב:',
			'all_items'           => 'כל הרכבים',
			'view_item'           => 'הצג רכב',
			'add_new_item'        => 'הוסף רכב חדש',
			'add_new'             => 'הוסף חדש',
			'edit_item'           => 'ערוך רכב',
			'update_item'         => 'עדכון רכב',
			'search_items'        => 'חפש רכב',
			'not_found'           => 'לא נמצא',
			'not_found_in_trash'  => 'לא מצא באשפה',
		);
		$rewrite = array(
			'slug'                => 'product',
			'with_front'          => true,
			'pages'               => true,
			'feeds'               => true,
		);
		$args = array(
			'label'               => 'product',
			'description'         => 'רכבים',
			'labels'              => $labels,
			'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail'),
			'taxonomies'          => array( 'product_cat', 'product_type','product_brand', 'product_engine' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'menu_icon'           => 'dashicons-products',
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'rewrite'             => $rewrite,
			'capability_type'     => 'post',
		);
		register_post_type( 'product', $args );

	}

	add_action( 'init', 'product_post_type', 0 );

	function product_taxonomy() {

		$labels = array(
			'name'                       => 'קטגוריות רכבים',
			'singular_name'              => 'קטגוריות רכבים',
			'menu_name'                  => 'קטגוריות רכבים',
			'all_items'                  => 'כל הקטגוריות',
			'parent_item'                => 'קטגורית הורה',
			'parent_item_colon'          => 'קטגורית הורה:',
			'new_item_name'              => 'שם קטגוריה חדשה',
			'add_new_item'               => 'להוסיף קטגוריה חדשה',
			'edit_item'                  => 'ערוך קטגוריה',
			'update_item'                => 'עדכן קטגוריה',
			'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
			'search_items'               => 'חיפוש קטגוריות',
			'add_or_remove_items'        => 'להוסיף או להסיר קטגוריות',
			'choose_from_most_used'      => 'בחר מהקטגוריות הנפוצות ביותר',
			'not_found'                  => 'לא נמצא',
		);
		$rewrite = array(
			'slug'                       => 'product_cat',
			'with_front'                 => true,
			'hierarchical'               => false,
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => $rewrite,
		);
		register_taxonomy( 'product_cat', array( 'product' ), $args );

	}

	add_action( 'init', 'product_taxonomy', 0 );

	function product_type() {

		$labels = array(
			'name'                       => 'סוגי רכבים',
			'singular_name'              => 'סוגי רכבים',
			'menu_name'                  => 'סוגי רכבים',
			'all_items'                  => 'כל הסוגים',
			'parent_item'                => 'סוג הורים',
			'parent_item_colon'          => 'סוג הורים:',
			'new_item_name'              => 'שם סוג חדש',
			'add_new_item'               => 'להוסיף סוג חדש',
			'edit_item'                  => 'ערוך סוג',
			'update_item'                => 'עדכן סוג',
			'separate_items_with_commas' => 'סוגים נפרדות עם פסיק',
			'search_items'               => 'חיפוש סוגים',
			'add_or_remove_items'        => 'להוסיף או להסיר סוגים',
			'choose_from_most_used'      => 'בחר מהסוגים הנפוצות ביותר',
			'not_found'                  => 'לא נמצא',
		);
		$rewrite = array(
			'slug'                       => 'product_type',
			'with_front'                 => true,
			'hierarchical'               => false,
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => $rewrite,
		);
		register_taxonomy( 'product_type', array( 'product' ), $args );

	}

	add_action( 'init', 'product_type', 0 );

	function product_brand() {

		$labels = array(
			'name'                       => 'יצרנים',
			'singular_name'              => 'יצרנים',
			'menu_name'                  => 'יצרנים',
			'all_items'                  => 'כל היצרנים',
			'parent_item'                => 'יצרן הורים',
			'parent_item_colon'          => 'יצרן הורים:',
			'new_item_name'              => 'שם יצרן חדש',
			'add_new_item'               => 'להוסיף יצרן חדש',
			'edit_item'                  => 'ערוך יצרן',
			'update_item'                => 'עדכן יצרן',
			'separate_items_with_commas' => 'יצרנים נפרדות עם פסיק',
			'search_items'               => 'חיפוש יצרנים',
			'add_or_remove_items'        => 'להוסיף או להסיר יצרנים',
			'choose_from_most_used'      => 'בחר מהיצרנים הנפוצות ביותר',
			'not_found'                  => 'לא נמצא',
		);
		$rewrite = array(
			'slug'                       => 'product_brand',
			'with_front'                 => true,
			'hierarchical'               => false,
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => $rewrite,
		);
		register_taxonomy( 'product_brand', array( 'product' ), $args );
	}
	add_action( 'init', 'product_brand', 0 );

	function product_engine() {

		$labels = array(
			'name'                       => 'מנוע',
			'singular_name'              => 'מנוע',
			'menu_name'                  => 'מנוע',
			'all_items'                  => 'כל המנועים',
			'parent_item'                => 'מנוע הורה',
			'parent_item_colon'          => 'מנוע הורה:',
			'new_item_name'              => 'שם מנוע חדש',
			'add_new_item'               => 'להוסיף מנוע חדש',
			'edit_item'                  => 'ערוך מנוע',
			'update_item'                => 'עדכן מנוע',
			'separate_items_with_commas' => 'מנועים נפרדות עם פסיק',
			'search_items'               => 'חיפוש מנועים',
			'add_or_remove_items'        => 'להוסיף או להסיר מנועים',
			'choose_from_most_used'      => 'בחר מהמנועים הנפוצים ביותר',
			'not_found'                  => 'לא נמצא',
		);
		$rewrite = array(
			'slug'                       => 'product_engine',
			'with_front'                 => true,
			'hierarchical'               => false,
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => $rewrite,
		);
		register_taxonomy( 'product_engine', array( 'product' ), $args );
	}
	add_action( 'init', 'product_engine', 0 );

	function product_box() {

		$labels = array(
			'name'                       => 'תיבות הילוכים',
			'singular_name'              => 'תיבות הילוכים',
			'menu_name'                  => 'תיבות הילוכים',
			'all_items'                  => 'כל התיבות הילוכים',
			'parent_item'                => 'תיבת הילוכים הורה',
			'parent_item_colon'          => 'תיבת הילוכים הורה:',
			'new_item_name'              => 'שם תיבת הילוכים חדשה',
			'add_new_item'               => 'להוסיף תיבת הילוכים חדשה',
			'edit_item'                  => 'ערוך תיבת הילוכים',
			'update_item'                => 'עדכן תיבת הילוכים',
			'separate_items_with_commas' => 'תיבות הילוכים עם פסיק',
			'search_items'               => 'חיפוש תיבות הילוכים',
			'add_or_remove_items'        => 'להוסיף או להסיר תיבות הילוכים',
			'choose_from_most_used'      => 'בחר מהתיבות הילוכים הנפוצים ביותר',
			'not_found'                  => 'לא נמצא',
		);
		$rewrite = array(
			'slug'                       => 'product_box',
			'with_front'                 => true,
			'hierarchical'               => false,
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'rewrite'                    => $rewrite,
		);
		register_taxonomy( 'product_box', array( 'product' ), $args );
	}
	add_action( 'init', 'product_box', 0 );

}

if(PROJECTS){
    function project_post_type() {

        $labels = array(
            'name'                => 'פרויקטים',
            'singular_name'       => 'פרויקטים',
            'menu_name'           => 'פרויקטים',
            'parent_item_colon'   => 'פריט אב:',
            'all_items'           => 'כל הפרויקטים',
            'view_item'           => 'הצג פרויקט',
            'add_new_item'        => 'הוסף פרויקט',
            'add_new'             => 'הוסף פרויקט חדש',
            'edit_item'           => 'ערוך פרויקט',
            'update_item'         => 'עדכון פרויקט',
            'search_items'        => 'חפש פריטים',
            'not_found'           => 'לא נמצא',
            'not_found_in_trash'  => 'לא מצא באשפה',
        );
        $rewrite = array(
            'slug'                => 'project',
            'with_front'          => true,
            'pages'               => true,
            'feeds'               => true,
        );
        $args = array(
            'label'               => 'project',
            'description'         => 'פרויקטים',
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail'),
            'taxonomies'          => array( 'project_cat' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-portfolio',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'rewrite'             => $rewrite,
            'capability_type'     => 'post',
        );
        register_post_type( 'project', $args );

    }

    add_action( 'init', 'project_post_type', 0 );

    function project_taxonomy() {

        $labels = array(
            'name'                       => 'קטגוריות פרויקטים',
            'singular_name'              => 'קטגוריות פרויקטים',
            'menu_name'                  => 'קטגוריות פרויקטים',
            'all_items'                  => 'כל הקטגוריות',
            'parent_item'                => 'קטגורית הורה',
            'parent_item_colon'          => 'קטגורית הורה:',
            'new_item_name'              => 'שם קטגוריה חדשה',
            'add_new_item'               => 'להוסיף קטגוריה חדשה',
            'edit_item'                  => 'ערוך קטגוריה',
            'update_item'                => 'עדכן קטגוריה',
            'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
            'search_items'               => 'חיפוש קטגוריות',
            'add_or_remove_items'        => 'להוסיף או להסיר קטגוריות',
            'choose_from_most_used'      => 'בחר מהקטגוריות הנפוצות ביותר',
            'not_found'                  => 'לא נמצא',
        );
        $rewrite = array(
            'slug'                       => 'project_cat',
            'with_front'                 => true,
            'hierarchical'               => false,
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
            'rewrite'                    => $rewrite,
        );
        register_taxonomy( 'project_cat', array( 'project' ), $args );

    }

    add_action( 'init', 'project_taxonomy', 0 );
}
if (SERVICES) {
	function service_post_type()
	{

		$labels = array(
			'name' => 'שירותים',
			'singular_name' => 'שירות',
			'menu_name' => 'שירותים',
			'parent_item_colon' => 'פריט אב:',
			'all_items' => 'כל השירותים',
			'view_item' => 'הצג שירות',
			'add_new_item' => 'הוסף שירות חדש',
			'add_new' => 'הוסף חדש',
			'edit_item' => 'ערוך שירות',
			'update_item' => 'עדכון שירות',
			'search_items' => 'חפש שירות',
			'not_found' => 'לא נמצא',
			'not_found_in_trash' => 'לא מצא באשפה',
		);
		$rewrite = array(
			'slug' => 'service',
			'with_front' => true,
			'pages' => true,
			'feeds' => true,
		);
		$args = array(
			'label' => 'service',
			'description' => 'שירותים',
			'labels' => $labels,
			'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail'),
			'taxonomies' => array('service_cat'),
			'hierarchical' => false,
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'show_in_nav_menus' => true,
			'show_in_admin_bar' => true,
			'menu_position' => 5,
			'menu_icon' => 'dashicons-businessman',
			'can_export' => true,
			'has_archive' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'rewrite' => $rewrite,
			'capability_type' => 'post',
		);
		register_post_type('service', $args);

	}

	add_action('init', 'service_post_type', 0);

	function service_taxonomy()
	{

		$labels = array(
			'name' => 'קטגוריות שירותים',
			'singular_name' => 'קטגוריות שירותים',
			'menu_name' => 'קטגוריות שירותים',
			'all_items' => 'כל השירותים',
			'parent_item' => 'קטגורית הורה',
			'parent_item_colon' => 'קטגורית הורה:',
			'new_item_name' => 'שם קטגוריה חדשה',
			'add_new_item' => 'להוסיף קטגוריה חדשה',
			'edit_item' => 'ערוך קטגוריה',
			'update_item' => 'עדכן קטגוריה',
			'separate_items_with_commas' => 'קטגוריות נפרדות עם פסיק',
			'search_items' => 'חיפוש קטגוריות',
			'add_or_remove_items' => 'להוסיף או להסיר קטגוריות',
			'choose_from_most_used' => 'בחר מהקטגוריות הנפוצות ביותר',
			'not_found' => 'לא נמצא',
		);
		$rewrite = array(
			'slug' => 'service_cat',
			'with_front' => true,
			'hierarchical' => false,
		);
		$args = array(
			'labels' => $labels,
			'hierarchical' => true,
			'public' => true,
			'show_ui' => true,
			'show_admin_column' => true,
			'show_in_nav_menus' => true,
			'show_tagcloud' => true,
			'rewrite' => $rewrite,
		);
		register_taxonomy('service_cat', array('service'), $args);

	}

	add_action('init', 'service_taxonomy', 0);
}
