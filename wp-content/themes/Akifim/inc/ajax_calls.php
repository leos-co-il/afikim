<?php

add_action('wp_ajax_nopriv_ajax_function', 'ajax_function');
add_action('wp_ajax_more_ajax_function', 'ajax_function');

function ajax_function()
{

    if (!wp_verify_nonce($_REQUEST['nonce'], "my_user_vote_nonce")) {
        exit("No naughty business please");
    }

    $result['type'] = "error";
    $result['var'] = 1;

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    } else {
        header("Location: " . $_SERVER["HTTP_REFERER"]);
    }
    die();
}

add_action('wp_ajax_nopriv_get_more_function', 'get_more_function');
add_action('wp_ajax_get_more_function', 'get_more_function');

function get_more_function() {
	$ids_string = (isset($_REQUEST['ids'])) ? $_REQUEST['ids'] : '';
	$id_term = (isset($_REQUEST['termID'])) ? $_REQUEST['termID'] : '';
	$id_page = (isset($_REQUEST['page'])) ? $_REQUEST['page'] : '';
	$count = (isset($_REQUEST['countAll'])) ? $_REQUEST['countAll'] : '';
	$quantityNow = (isset($_REQUEST['quantity'])) ? $_REQUEST['quantity'] : '';
	$type = (isset($_REQUEST['postType'])) ? $_REQUEST['postType'] : 'post';
	var_dump($type);
	$termName = (isset($_REQUEST['termName'])) ? $_REQUEST['termName'] : 'category';
	$ids = explode(',', $ids_string);
	$params_string = (isset($_REQUEST['params'])) ? $_REQUEST['params'] : '';
	$hi = json_decode('"'.$params_string.'"', true, 4);
	$html = '';
	$result['html'] = '';
	if ($type === 'post') {
		$query = new WP_Query([
			'post_type' => 'post',
			'posts_per_page' => 4,
			'post__not_in' => $ids,
			'suppress_filters' => false,
			'tax_query' => $id_term && $termName? [
				[
					'taxonomy' => $termName,
					'field' => 'term_id',
					'terms' => $id_term,
				]
			] : '',
		]);
		if ($query->have_posts()) {
			foreach ($query->posts as $item) {
				$html = load_template_part('views/partials/card', 'post', [
					'post' => $item,
				]);
				$result['html'] .= $html;
				if ($count <= ($quantityNow + 4)) {
					$result['quantity'] = true;
				}
			}
		}
	} else {
		$hi['post__not_in'] = $ids;
		$hi['posts_per_page'] = 4;
		$hi['post_type'] = $type;
		$query = new WP_Query($hi);
		if ($query->have_posts()) {
			foreach ($query->posts as $item) {
				$html = load_template_part('views/partials/card', 'product', [
					'product' => $item,
				]);
				$result['html'] .= $html;
				if ($count <= ($quantityNow + 4)) {
					$result['quantity'] = true;
				}
			}
		}
	}
	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}
add_action('wp_ajax_nopriv_models_search', 'models_search');
add_action('wp_ajax_models_search', 'models_search');


function models_search()
{

	$result['type'] = "success";

	$term_id = (isset($_REQUEST['brand'])) ? $_REQUEST['brand'] : '';
	$terms = get_term_children( $term_id, 'product_brand');
	$html = [];
	foreach ($terms as $term) {
		$term_name = get_term($term)->name;
		$html[$term] = $term_name;
//		$html = load_template_part('views/partials/card', 'option', [
//			'item' => $term,
//		]);
	}
	$result['html'] = $html;

	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}
