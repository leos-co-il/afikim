(function($) {
	$.fn.slideFadeToggle  = function(speed, easing, callback) {
		return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
	};
	$('#inputBrand').change(function() {
		var brand = '';
		$( '#inputBrand option:selected').each(function() {
			brand = $( this ).data('id');
		});
		var select = $('.form-group').find($('#inputModel'));
		$('#inputModel').children('option:not(:first)').remove();
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			type: 'post',
			dataType: 'json',
			data: {
				brand: brand,
				action: 'models_search',
			},
			success: function (data) {

				var options = data.html;
				console.log(options);
				$.each(options, function (i, item) {
					select.append($('<option>', {
						value: i,
						text : item
					}));
				});
			}
		});
	});
	$('.pop-great').click(function() {
		$(this).removeClass('show-popup');
		$('.float-form').removeClass('show-float-form');
	});

	$('.pop-great').click(function(event){
		event.stopPropagation();
	});
	$( document ).ready(function() {
		var menu = $( '.drop-menu' );
		$('.hamburger').click(function () {
			menu.slideFadeToggle();
			$(this).toggleClass('is-active');
		});
		$( '.rev-pop-trigger' ).click( function() {
			var contentAll = $( this ).children('.hidden-review').html();
			$( '#reviews-pop-wrapper' ).html( contentAll );
			$( '#reviewsModal' ).modal( 'show' );
		});
		$( '#reviewsModal' ).on( 'hidden.bs.modal', function( e ) {
			$( '#reviews-pop-wrapper' ).html( '' );
		});
		$('.cat-item-img').hover(function (){
			$(this).children('.cat-item-info').children('.cat-item-info-part').slideFadeToggle();
		});
		$('.pop-trigger').click(function () {
			$('.pop-great').toggleClass('show-popup');
			$('.float-form').toggleClass('show-float-form');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$('.logos-slider').slick({
			slidesToShow: 8,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 3000,
			rtl: true,
			arrows: false,
			dots: false,
			responsive: [
				{
					breakpoint: 1600,
					settings: {
						slidesToShow: 6,
					}
				},
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 5,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 4,
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 2,
					}
				},
			]
		});
		$('.main-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: false,
			dots: true,
		});
		$('.gallery-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			rtl: true,
			asNavFor: '.thumbs'
		});
		$('.thumbs').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			rtl: true,
			asNavFor: '.gallery-slider',
			dots: false,
			arrows: true,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 2,
					}
				},
				// {
				// 	breakpoint: 992,
				// 	settings: {
				// 		slidesToShow: 2,
				// 	}
				// },
				// // {
				// // 	breakpoint: 768,
				// // 	settings: {
				// // 		slidesToShow: 2,
				// // 	}
				// // },
			]
		});
		$('.products-slider').slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
			responsive: [
				// {
				// 	breakpoint: 1200,
				// 	settings: {
				// 		slidesToShow: 4,
				// 	}
				// },
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 500,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$('.reviews-slider').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-button').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('#iframe-wrapper').html(frame);
			$('#modalCenter').modal('show');
		});
		$('#modalCenter').on('hidden.bs.modal', function (e) {
			$('#iframe-wrapper').html('');
		});
		$('.play-button-post').click(function() {
			var id = $(this).data('id');
			var iFrame = '<iframe src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('.put-video-here').addClass('show').html(iFrame);
		});
		var accordion = $('#accordion-product');
		accordion.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().children('.button-prod').children('.plus-icon').addClass('hide');
			show.parent().children('.button-prod').children('.minus-icon').addClass('show');
		});
		accordion.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().children('.button-prod').children('.minus-icon').removeClass('show');
			collapsed.parent().children('.button-prod').children('.plus-icon').removeClass('hide');
		});
		//Socials
		$('.trigger-wrap').hover(function(){
			$(this).children('.all-socials').addClass('show-share');
		}, function(){
			$(this).children('.all-socials').removeClass('show-share');
		});
	});
	$('.load-more-posts').click(function(e) {
		e.preventDefault();
		var btn = $(this);
		btn.addClass('loading');
		btn.append('<div class="cart-loading"><i class="fas fa-spinner fa-pulse"></i></div>');
		var postType = $(this).data('type');
		var termID = $(this).data('term');
		var termName = $(this).data('term_name');
		var params = $('.take-json').html();
		var ids = '';
		var page = $(this).data('page');
		var countAll = $(this).data('count');
		var quantity = $('.more-card').length;
		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				postType: postType,
				termID: termID,
				params: params,
				termName: termName,
				ids: ids,
				page: page,
				quantity: quantity,
				countAll: countAll,
				// taxType: taxType,
				action: 'get_more_function',
			},
			success: function (data) {
				btn.removeClass('loading');
				$('.cart-loading').remove();
				if (!data.html || data.quantity) {
					btn.addClass('hide');
				}
				$('.put-here-posts').append(data.html);
			}
		});
	});
})( jQuery );
