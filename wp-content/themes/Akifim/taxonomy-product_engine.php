<?php

get_header();
$query = get_queried_object();
$cars = new WP_Query([
	'posts__per_page' => 8,
	'post_type' => 'product',
	'tax_query' => array(
		array(
			'taxonomy' => 'product_engine',
			'field' => 'term_id',
			'terms' => $query->term_id,
		)
	)
]);
$published_posts = new WP_Query([
	'posts_per_page' => -1,
	'post_type' => 'product',
	'suppress_filters' => false,
	'tax_query' => array(
		array(
			'taxonomy' => 'product_engine',
			'field' => 'term_id',
			'terms' => $query->term_id,
		)
	)
]);
?>
<article class="page-body">
	<div class="container pt-4">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="block-title"><?= $query->name; ?></h1>
			</div>
			<div class="col-12">
				<div class="base-output">
					<?= category_description(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php get_template_part('views/partials/repeat', 'search');
	if ($cars->have_posts()) : ?>
		<div class="container products-results">
			<div class="row justify-content-center">
				<div class="col-12">
					<div class="row products-row put-here-posts">
						<?php foreach ($cars->posts as $num => $product) : ?>
							<?php get_template_part('views/partials/card', 'product', [
								'product' => $product,
							]); ?>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	<?php else : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col">
					<h3 class="block-title text-center mb-2">
						אין רכבים מתאימות לחיפוש שלך
					</h3>
					<div class="row justify-content-center">
						<div class="col-auto mt-3">
							<a class="base-link" href="<?= opt('search_page')['url']; ?>">
								לכל הרכבים
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<?php if (($cars->have_posts() && ($cars_num = count($published_posts->posts)) > 8)) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="base-link more-link load-more-posts" data-type="product" data-count="<?= $cars_num; ?>"
						 data-term_name="product_engine" data-term="<?= $query->term_id; ?>">טען עוד..</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php get_footer(); ?>
