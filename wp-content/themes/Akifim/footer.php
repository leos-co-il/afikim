<?php

$contact = opt('contact');
$mail = opt('mail');
$address = opt('address');
$open_hours = opt('open_hours');
?>

<footer>
	<div class="footer-main">
		<a id="go-top">
			<img src="<?= ICONS ?>to-top.png" alt="go-top">
			<span class="to-top-text">חזרה למעלה</span>
		</a>
		<div class="foo-form-wrap">
			<div class="container">
				<div class="row justify-content-center mb-4">
					<div class="col-auto">
						<?php if ($base_form_title = opt('foo_form_title')) : ?>
							<h2 class="form-title"><?= $base_form_title; ?></h2>
						<?php endif; ?>
					</div>
					<div class="col-auto">
						<?php if ($base_form_subtitle = opt('foo_form_subtitle')) : ?>
							<h2 class="form-subtitle"><?= $base_form_subtitle; ?></h2>
						<?php endif; ?>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-12">
						<div class="middle-form wow zoomIn">
							<?php getForm('196'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container footer-container-menu">
			<div class="row justify-content-between align-items-start">
				<div class="col-xl-3 col-lg-auto col-sm-6 col-12 foo-menu">
					<h3 class="foo-title">
						<?= esc_html__('תפריט ראשי //', 'leos'); ?>
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-menu', '1'); ?>
					</div>
				</div>
				<div class="col-lg col-12 foo-menu foo-links-menu">
					<?php $foo_l_title = opt('foo_menu_title'); ?>
					<h3 class="foo-title">
						<?php echo $foo_l_title ? $foo_l_title : esc_html__('תפריט עם קישורים //', 'leos'); ?>
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-links-menu', '1', 'hop-hey three-columns'); ?>
					</div>
				</div>
				<div class="col-xl-2 col-lg-3 col-sm-6 foo-menu contacts-footer-menu">
					<h3 class="foo-title">
						<?= esc_html__('פרטי התקשרות // ', 'leos'); ?>
					</h3>
					<div class="menu-border-top">
						<ul class="contact-list d-flex flex-column">
							<?php if ($contact) : ?>
								<li>
									<?= svg_simple(ICONS.'foo-tel.svg'); ?>
									<div>
										<?php foreach ($contact as $tel) : if ($tel['tel']) : ?>
											<a href="tel:<?= $tel['tel']; ?>" class="contact-info-footer">
												<?php echo $tel['tel']; echo (isset($tel['name']) && $tel['name']) ? $tel['name'] : '' ?>
											</a>
										<?php endif; endforeach; ?>
									</div>
								</li>
							<?php endif; ?>
							<?php if ($address) : ?>
								<li>
									<?= svg_simple(ICONS.'foo-geo.svg'); ?>
									<a href="https://waze.com/ul?q=<?= $address; ?>"
									   class="contact-info-footer" target="_blank">
										<?= $address; ?>
									</a>
								</li>
							<?php endif; ?>
							<?php if ($mail) : ?>
								<li>
									<img src="<?= ICONS ?>foo-mail.png" alt="mail">
									<a href="mailto:<?= $mail; ?>" class="contact-info-footer">
										<?= $mail; ?>
									</a>
								</li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>

<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
    require_once THEMEPATH . "/inc/debug.php"
    ?>
    <script>

        function _fetchHeader($_el){
            let res = {
                'count' : 0,
                'content' : ''
            } ;
            $($_el).each(function () {
                res.count++;
                res.content += ' [' + $(this).text() + '] ';
            });
            return 'Count: ' + res.count + '. Text: ' + res.content;
        }

        function _fetchMeta($_meta){
            return $('meta[name='+$_meta+']').attr("content");
        }




        phpdebugbar.addDataSet({
            "SEO Local": {
                'H1' : _fetchHeader('h1'),
                'H2' : _fetchHeader('h2'),
                'H3' : _fetchHeader('h3'),
                'Meta Title' : _fetchMeta('title'),
                'Meta Description' : _fetchMeta('description'),
                'Meta Keywords' : _fetchMeta('keywords'),
            }
        });
    </script>

<?php endif; ?>

</body>
</html>
