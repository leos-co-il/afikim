<?php
/*
Template Name: אודות
*/

the_post();
get_header();
$fields = get_fields();

?>


<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container pt-4">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="base-output text-center">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
</article>
<div class="benefits-back">
	<?php get_template_part('views/partials/repeat', 'benefits', [
		'img' => $fields['benefits_image'],
		'benefits' => $fields['benefits'],
	]); ?>
</div>
<?php get_template_part('views/partials/repeat', 'form');
get_template_part('views/partials/content', 'reviews',
	[
		'reviews' => $fields['reviews'],
		'title' => $fields['reviews_title'],
	]);
if ($fields['single_slider_seo']) :
	get_template_part('views/partials/content', 'slider',
		[
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
		]);
endif;
get_footer(); ?>
