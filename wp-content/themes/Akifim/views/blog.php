<?php
/*
Template Name: מאמרים
*/

get_header();
$fields = get_fields();
$posts = new WP_Query([
	'posts_per_page' => 8,
	'post_type' => 'post',
	'suppress_filters' => false
]);
$published_posts = new WP_Query([
	'posts_per_page' => -1,
	'post_type' => 'post',
	'suppress_filters' => false,
]);
get_template_part('views/partials/repeat', 'breadcrumbs');
?>
	<article class="page-body mb-4">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<h1 class="block-title">
						<?php the_title(); ?>
					</h1>
				</div>
				<div class="col-12">
					<div class="base-output text-center">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
			<?php if ($posts->have_posts()) : ?>
				<div class="row align-items-stretch put-here-posts justify-content-center">
					<?php foreach ($posts->posts as $post) {
						get_template_part('views/partials/card', 'post',
							[
								'post' => $post,
							]);
					} ?>
				</div>
			<?php endif;
			if ($published_posts->have_posts() && (($num = count($published_posts->posts)) > 8)) : ?>
				<div class="row justify-content-center mt-4 mb-5">
					<div class="col-auto">
						<div class="base-link more-link load-more-posts" data-type="post" data-count="<?= $num; ?>">
							<span><?= esc_html__('טען עוד...', 'leos'); ?></span>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</article>
<?php get_template_part('views/partials/repeat', 'form');
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
