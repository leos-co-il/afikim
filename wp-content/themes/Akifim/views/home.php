<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>

<section class="home-main-block">
	<span class="main-overlay"></span>
	<?php if ($fields['h_main_slider']) : ?>
		<div class="main-slider" dir="rtl">
			<?php foreach ($fields['h_main_slider'] as $slide) : ?>
				<?php $img = ''; $video = '';
				if (isset($slide['main_back']['0'])) {
					if ($slide['main_back']['0']['acf_fc_layout'] === 'main_back_img') {
						$img = isset($slide['main_back']['0']['img']) ? $slide['main_back']['0']['img'] : '';
					} elseif ($slide['main_back']['0']['acf_fc_layout'] === 'main_back_video') {
						$video = isset($slide['main_back']['0']['video']) ? $slide['main_back']['0']['video'] : '';
					}
				}
				if ($video) : ?>
					<div class="slide-video">
						<video muted autoplay="autoplay" loop="loop">
							<source src="<?= $video['url']; ?>" type="video/mp4">
						</video>
					</div>
				<?php else: ?>
					<div class="slide-main" style="background-image: url('<?= $img ? $img['url'] : ''; ?>')">
					</div>
				<?php endif;
			endforeach; ?>
		</div>
	<?php endif; ?>
	<div class="main-content-block">
		<div class="container">
			<?php if ($fields['h_main_logo']) : ?>
				<div class="row justify-content-center">
					<div class="col-lg-4 col-md-5 col-sm-6 col-8">
						<img src="<?= $fields['h_main_logo']['url']; ?>" alt="logo-image">
					</div>
				</div>
			<?php endif; ?>
			<?php if ($fields['h_main_title']) : ?>
				<div class="row justify-content-start">
					<div class="col-12">
						<h1 class="homepage-main-title">
							<?= $fields['h_main_title']; ?>
						</h1>
					</div>
				</div>
			<?php endif;
			if ($fields['h_main_links']) : ?>
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($fields['h_main_links'] as $link) : if ($link['url']) : ?>
						<div class="col-auto homepage-main-link-col">
							<a href="<?= $link['url']['url']; ?>" class="homepage-main-link">
								<?= isset($link['url']['title']) ? $link['url']['title'] : ''; ?>
							</a>
						</div>
					<?php endif; endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>
<div class="search-home">
	<?php get_template_part('views/partials/repeat', 'search'); ?>
</div>
<?php if ($fields['h_products']) : ?>
	<section class="slider-products-block arrows-slider arrows-slider-pro">
		<div class="container">
			<div class="row">
				<?php if ($fields['h_products_title']) : ?>
					<div class="col-12">
						<h2 class="block-title mb-3"><?= $fields['h_products_title']; ?></h2>
					</div>
				<?php endif;
				if ($fields['h_products_links']) : ?>
					<div class="col-12">
						<div class="row justify-content-center align-items-center">
							<?php foreach ($fields['h_products_links'] as $m => $link) : if ($link['link']) : ?>
								<div class="col-auto prod-link-col">
									<a href="<?= $link['link']['url']; ?>" class="prod-block-link">
										<?php echo isset($link['link']['title']) ? $link['link']['title'] : ''; ?>
									</a>
								</div>
							<?php endif; endforeach; ?>
						</div>
					</div>
				<?php endif; ?>
				<div class="col-12">
					<div class="products-slider" dir="rtl">
						<?php foreach ($fields['h_products'] as $product) : ?>
							<div class="py-3">
								<?php get_template_part('views/partials/card', 'product', [
										'product' => $product,
								]); ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['h_services']) : ?>
	<section class="home-services">
		<div class="container">
			<div class="row">
				<?php if ($fields['h_services_title']) : ?>
					<div class="col-12">
						<h2 class="block-title"><?= $fields['h_services_title']; ?></h2>
					</div>
				<?php endif; ?>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($fields['h_services'] as $service) : $service_link = get_the_permalink($service); ?>
					<div class="col-md-4 col-12 mb-md-0 mb-5 d-flex flex-column">
						<a class="service-image" href="<?= $service_link; ?>">
							<?php if (has_post_thumbnail($service)) : ?>
								<img src="<?= postThumb($service); ?>" alt="service-image">
							<?php endif; ?>
						</a>
						<a class="service-title" href="<?= $service_link; ?>"><?= $service->post_title; ?></a>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'form'); ?>
<section class="about-block">
	<div class="container">
		<div class="row justify-content-center">
			<?php if ($fields['h_about_text']) : ?>
				<div class="col-xl-10 col-md-11 col-12">
					<div class="base-output text-center"><?= $fields['h_about_text']; ?></div>
				</div>
			<?php endif; ?>
		</div>
		<?php if ($fields['h_about_link']) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<a href="<?= isset($fields['h_about_link']['url']) ? $fields['h_about_link']['url'] : ''; ?>" class="base-link">
						<span>
							<?= isset($fields['h_about_link']['title']) ? $fields['h_about_link']['title'] : 'קרא עוד עלינו'; ?>
						</span>
						<img src="<?= ICONS ?>arrow-link.png" alt="read-more" class="mr-3">
					</a>
				</div>
			</div>
		<?php endif; ?>
	</div>
</section>
<?php get_template_part('views/partials/repeat', 'benefits');
get_template_part('views/partials/content', 'reviews');
if ($fields['h_brands']) : ?>
	<div class="partners-line">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 prod-slider-col">
					<div class="logos-slider" dir="rtl">
						<?php foreach ($fields['h_brands'] as $partner) : if ($partner['img']) : ?>
							<div class="logo-slide-item">
								<a class="logo-car-col" <?= ($partner['link'] && isset($partner['link']['0'])) ? get_term_link($partner['link']['0']) : ''; ?>>
									<img src="<?= $partner['img']['url']; ?>" alt="logo">
								</a>
							</div>
						<?php endif; endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif;
if ($fields['h_posts']) : ?>
	<section class="posts-home-block">
		<div class="container">
			<div class="row">
				<?php if ($fields['h_posts_title']) : ?>
					<div class="col-12">
						<h2 class="block-title"><?= $fields['h_posts_title']; ?></h2>
					</div>
				<?php endif; ?>
			</div>
			<?php foreach ($fields['h_posts'] as $post) : $link = get_the_permalink($post); ?>
				<div class="row row-tips">
					<?php if (has_post_thumbnail($post)) : ?>
						<a class="col-lg-6 post-link-image" href="<?= $link; ?>" style="background-image: url('<?= postThumb($post); ?>')">
						</a>
					<?php endif; ?>
					<div class="col-lg-6 col-post-content d-flex flex-column">
						<div class="base-output">
							<h4>
								<a href="<?= $link; ?>"><?= $post->post_title; ?></a>
							</h4>
						</div>
						<p class="base-text mb-3">
							<?= text_preview($post->post_content, 50); ?>
						</p>
						<a href="<?= $link; ?>" class="post-link align-self-end">
							המשך קריאה
						</a>
					</div>
				</div>
			<?php endforeach;
			if ($fields['h_posts_link']) : ?>
				<div class="row justify-content-end">
					<div class="col-auto">
						<a href="<?= isset($fields['h_posts_link']['url']) ? $fields['h_posts_link']['url'] : ''; ?>" class="base-link">
						<span>
							<?= isset($fields['h_posts_link']['title']) ? $fields['h_posts_link']['title'] : 'לכל הכתבות'; ?>
						</span>
							<img src="<?= ICONS ?>arrow-link.png" alt="read-more" class="mr-3">
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
