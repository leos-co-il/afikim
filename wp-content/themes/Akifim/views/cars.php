<?php
/*
Template Name: רכבים
*/

get_header();
$fields = get_fields();
$_order_type = (isset($_GET['order-type'])) ? $_GET['order-type'] : null;
$_car_type = (isset($_GET['car-type'])) ? $_GET['car-type'] : null;
$_brands = (isset($_GET['car-brand'])) ? $_GET['car-brand'] : null;
$_years = (isset($_GET['car-year'])) ? $_GET['car-year'] : null;
$_engines = (isset($_GET['car-engine'])) ? $_GET['car-engine'] : null;
$_boxes = (isset($_GET['car-box'])) ? $_GET['car-box'] : null;


$_price_from = (isset($_GET['price-from'])) ? intval($_GET['price-from']) : 0;
$_price_to = (isset($_GET['price-to']) && !empty($_GET['price-to'])) ? intval($_GET['price-to']) : PHP_INT_MAX;

$_year_from = (isset($_GET['year-from'])) ? intval($_GET['year-from']) : 0;
$_year_to = (isset($_GET['year-to']) && !empty($_GET['year-to'])) ? intval($_GET['year-to']) : PHP_INT_MAX;
$_query = (isset($_GET['search-query'])) ? sanitize_text_field($_GET['search-query']) : null;

$query_args = [
    'post_type' => 'product',
    'posts_per_page' => 8,
    'meta_query' => [
        [
            'key' => 'year',
            'value'   => [$_year_from, $_year_to],
            'type'    => 'numeric',
            'compare' => 'BETWEEN',
        ],
        [
            'key' => 'price',
            'value'   => [$_price_from, $_price_to],
            'type'    => 'numeric',
            'compare' => 'BETWEEN',
        ],
    ]
];

if($_order_type || $_car_type || $_brands || $_years || $_engines || $_boxes){
    $query_args['tax_query'] = [
        'relation' => 'AND',
        $_car_type ? [
            'taxonomy' => 'product_type',
            'field'    => 'term_id',
            'terms'    => [$_car_type],
        ] : null,
        $_brands ? [
            'taxonomy' => 'product_brand',
            'field'    => 'term_id',
            'terms'    => $_brands,
        ] : null,
        $_engines ? [
            'taxonomy' => 'product_engine',
            'field' => 'term_id',
            'terms' => [$_engines],
        ] : null,
        $_boxes ? [
            'taxonomy' => 'product_box',
            'field' => 'term_id',
            'terms' => [$_boxes],
        ] : null,
    ];
}else{
    $query_args['tax_query'] = null;
}

if($_query){
    $query_args['s'] = $_query;
}
$cars = new WP_Query($query_args);
$json = json_encode($query_args);
$published_posts = new WP_Query([
		'posts_per_page' => -1,
		'post_type' => 'product',
		'suppress_filters' => false,
]);
get_template_part('views/partials/repeat', 'breadcrumbs');
?>
<article class="page-body">
	<div class="container pt-4">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="block-title"><?php the_title(); ?></h1>
			</div>
			<div class="col-12">
				<div class="base-output">
                    <?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
    <?php get_template_part('views/partials/repeat', 'search');
	if ($cars->have_posts()) : ?>
		<div class="container products-results">
			<div class="row justify-content-center">
				<div class="col-12">
					<div class="row products-row put-here-posts">
                        <?php foreach ($cars->posts as $num => $product) : ?>
                            <?php get_template_part('views/partials/card', 'product', [
                                'product' => $product,
                            ]); ?>
                        <?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
    <?php else : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col">
					<h3 class="block-title text-center mb-2">
						אין רכבים מתאימות לחיפוש שלך
					</h3>
					<div class="row justify-content-center">
						<div class="col-auto mt-3">
							<a class="base-link" href="<?php the_permalink(); ?>">
								לכל הרכבים
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
    <?php endif; ?>
    <?php if (($cars->have_posts() && ($cars_num = count($cars->posts)) > 8)) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="base-link more-link load-more-posts" data-type="product" data-count="<?= $cars_num; ?>">טען עוד..</div>
					<span class="take-json"><?= $json; ?></span>
				</div>
			</div>
		</div>
    <?php endif; ?>
</article>
<?php get_footer(); ?>
