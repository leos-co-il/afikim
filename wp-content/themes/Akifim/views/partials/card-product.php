<?php if (isset($args['product']) && $args['product']) : $post = $args['product'];
	$postId = $post->ID;
	$price = number_format(get_field('price', $postId));
	$run = number_format(get_field('kilometers', $postId));
	$doors = get_field('doors', $postId);
	$sits = get_field('seats', $postId);
	$transmission = wp_get_object_terms( $postId, 'product_box', ['fields'=>'names']);
	$volume = get_field('type_engine', $postId);
	$year = get_field('year', $postId);
	$engine = wp_get_object_terms( $postId, 'product_engine', ['fields'=>'names']);
	$info = [
		['img' => 'doors', 'val' => $doors.' '.'דלתות'],
		['img' => 'seats', 'val' => $sits.' '.'מושבים'],
		['img' => 'type', 'val' => array_shift($transmission).' '.'גיר'],
		['img' => 'engine', 'val' => $volume.' '.'מנוע'],
		['img' => 'year', 'val' => $year.' '.'שנה'],
		['img' => 'gas', 'val' => array_shift($engine)],
	];
	?>
	<div class="prod-col col-xl-3 col-sm-6 col-12">
		<a class="post-card product-card more-card" href="<?php the_permalink($post); ?>" data-id="<?= $postId; ?>">
			<div class="post-inside-image product-card-image">
				<?php if (has_post_thumbnail($post)) : ?>
					<img src="<?= postThumb($post); ?>" alt="car-image">
				<?php endif;?>
			</div>
			<div class="product-card-content">
				<div class="d-flex justify-content-center align-items-center border-card card-title-wrap">
					<h3 class="prod-card-title"><?= $post->post_title; ?></h3>
				</div>
				<div class="row border-card">
					<?php foreach ($info as $item) : ?>
						<?php if ($item['val']) : ?>
							<div class="col-4 info-card-item">
								<div class="info-card-icon">
									<img src="<?= ICONS.$item['img'] ?>.png">
								</div>
								<p class="small-card-info">
									<?= $item['val']; ?>
								</p>
							</div>
						<?php endif; ?>
					<?php endforeach; ?>
				</div>
				<div class="price-card-info">
					<?php if ($price) : ?>
						<span class="prod-card-title price-title mb-0"><?= 'מחיר'.' '.'₪'.$price; ?></span>
					<?php endif; ?>
					<?php if ($run) : ?>
						<span class="prod-card-title price-title mb-0"><?= $run.' '.'ק”מ'; ?></span>
					<?php endif; ?>
				</div>
			</div>
		</a>
	</div>
<?php endif; ?>
