<?php
$reviews = isset($args['reviews']) && $args['reviews'] ? $args['reviews'] : opt('reviews');
$title = isset($args['title']) && $args['title'] ? $args['title'] : opt('reviews_title');
if ($reviews) : ?>
<div class="reviews-block arrows-slider">
	<div class="container">
		<div class="row justify-content-center align-items-center">
			<div class="col-12">
				<h2 class="block-title"><?= $title; ?></h2>
			</div>
			<div class="col-12">
				<div class="reviews-slider" dir="rtl">
					<?php foreach ($reviews as $x => $review) : ?>
						<div class="review-slide">
							<div class="review-item-wrap">
								<div class="review-item">
									<img src="<?= ICONS ?>quote-top.png" alt="quotes" class="quote quote-top">
									<img src="<?= ICONS ?>quote-bottom.png" alt="quotes" class="quote quote-bottom">
									<div class="rev-content">
										<h3 class="review-title"><?= $review['name']; ?></h3>
										<div class="review-prev-content">
											<p class="review-prev">
												<?= text_preview($review['text'], '30'); ?>
											</p>
										</div>
										<div class="rev-pop-trigger">
											+
											<div class="hidden-review">
												<div class="base-output">
													<?= $review['text']; ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</div>
	<div class="modal fade" id="reviewsModal" tabindex="-1" role="dialog"
		 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
				<div class="modal-body" id="reviews-pop-wrapper"></div>
			</div>
		</div>
	</div>
<?php endif; ?>
