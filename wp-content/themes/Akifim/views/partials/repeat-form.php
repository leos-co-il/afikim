<?php
$base_form_title = (isset($args['title']) && $args['title']) ? $args['title'] : opt('base_form_title');
$base_form_subtitle = (isset($args['subtitle']) && $args['subtitle']) ? $args['subtitle'] : opt('base_form_subtitle');
$id = (isset($args['id']) && $args['id']) ? $args['id'] : '197';
?>
<section class="repeat-form-block">
	<div class="container">
		<div class="row justify-content-center mb-3">
			<div class="col-auto">
				<?php if ($base_form_title) : ?>
					<h2 class="form-title"><?= $base_form_title; ?></h2>
				<?php endif; ?>
			</div>
			<div class="col-auto">
				<?php if ($base_form_subtitle) : ?>
					<h2 class="form-subtitle"><?= $base_form_subtitle; ?></h2>
				<?php endif; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<?php getForm($id); ?>
			</div>
		</div>
	</div>
</section>
