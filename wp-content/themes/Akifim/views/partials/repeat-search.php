<?php
$cats = get_terms([
	'taxonomy' => 'product_type',
	'hide_empty' => false,
]);
$brands = get_terms([
	'taxonomy' => 'product_brand',
	'hide_empty' => false,
	'parent' => 0,
]);
$engines = get_terms([
	'taxonomy' => 'product_engine',
	'hide_empty' => false,
]);
$boxes = get_terms([
	'taxonomy' => 'product_box',
	'hide_empty' => false,
]);
?>

<div class="property-search-section">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="car-search-wrap">
					<form method="get" action="<?= opt('search_page')['url']; ?>">
						<div class="row align-items-end">
							<?php if ($cats): ?>
								<div class="form-group col-xl col-sm-6">
									<select id="inputTypeCar" name="car-type" class="form-control">
										<option selected disabled><?= esc_html__('בחר סוג רכב','leos')  ?></option>
										<?php foreach($cats as $c): ?>
											<option value="<?= $c->term_id ?>"><?= $c->name ?></option>
										<?php endforeach ?>
									</select>
								</div>
							<?php endif;
							if ($brands): ?>
								<div class="form-group col-xl col-sm-6">
									<select id="inputBrand" name="car-brand" class="form-control">
										<option selected disabled><?= esc_html__('בחר יצרן','leos')  ?></option>
										<?php foreach($brands as $brand): ?>
											<option value="<?= $brand->term_id ?>" data-id="<?= $brand->term_id ?>">
												<?= $brand->name ?>
											</option>
										<?php endforeach ?>
									</select>
								</div>
							<?php endif; ?>
							<div class="form-group col-xl col-sm-6">
								<select id="inputModel" name="car-model" class="form-control">
									<option selected disabled><?= esc_html__('בחר דגם','leos')  ?></option>
								</select>
							</div>
							<div class="form-group col-xl col-sm-6 price-group">
								<div class="input-group">
									<input type="text" class="form-control" id="inputYear" name="year-from" placeholder="משנה">
									<input type="text" class="form-control" id="inputYear2" name="year-to" placeholder="עד שנה">
								</div>
							</div>
						</div>
						<div class="row">
							<?php  if ($engines): ?>
								<div class="form-group col-xl col-sm-6">
									<select id="inputTypeCar" name="car-type" class="form-control">
										<option selected disabled><?= esc_html__('בחר סוג מנוע','leos')  ?></option>
										<?php foreach($engines as $engine): ?>
											<option value="<?= $engine->term_id ?>"><?= $engine->name ?></option>
										<?php endforeach ?>
									</select>
								</div>
							<?php endif;
							if ($boxes): ?>
								<div class="form-group col-xl col-sm-6">
									<select id="inputTypeCar" name="car-type" class="form-control">
										<option selected disabled><?= esc_html__('בחר תיבת הילוכים','leos')  ?></option>
										<?php foreach($boxes as $box): ?>
											<option value="<?= $box->term_id ?>"><?= $box->name ?></option>
										<?php endforeach ?>
									</select>
								</div>
							<?php endif; ?>
							<div class="form-group col-xl col-sm-6 price-group">
								<div class="input-group">
									<input type="text" class="form-control" id="inputPrice" name="price-from" placeholder="ממחיר">
									<input type="text" class="form-control" id="inputPrice2" name="price-to" placeholder="עד מחיר">
								</div>
							</div>
							<div class="form-group col-xl col-sm-6 d-flex align-items-end form-buttons">
								<button type="submit" class="btn w-100 btn-search"><?= esc_html__('חפש רכב','leos') ?></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
