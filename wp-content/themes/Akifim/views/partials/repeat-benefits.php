<?php
$img_diagonal = isset($args['benefits_image']) && $args['benefits_image'] ? $args['benefits_image'] : opt('benefits_image');
$why_us = isset($args['benefits']) && $args['benefits'] ? $args['benefits'] : opt('benefits');
?>
<div class="benefits-block">
	<div class="container">
		<div class="row justify-content-center align-items-end">
			<?php if ($why_us) : ?>
				<div class="col-xl-5 col-lg-7 col-12">
					<?php foreach ($why_us as $x => $why_item) : ?>
						<div class="why-item">
							<?php if ($why_icon = $why_item['icon']) : ?>
								<div class="why-icon">
									<img src="<?= $why_icon['url']; ?>" alt="benefit-icon">
								</div>
							<?php endif; ?>
							<div class="why-content">
								<h3 class="why-title"><?= $why_item['title']; ?></h3>
								<p class="base-text"><?= $why_item['desc']; ?></p>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; if ($img_diagonal) : ?>
				<div class="col-xl-7 col-lg-5 col-12">
					<img src="<?= $img_diagonal['url']; ?>" alt="image-benefits" class="w-100">
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
