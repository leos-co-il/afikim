<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif;
$front = is_front_page(); ?>


<header class="sticky <?= $front ? 'front-header' : ''; ?>">
	<div class="container-fluid <?= $front ? 'hide-on-front' : ''; ?>">
		<div class="row justify-content-center">
			<div class="col-xl-11">
				<div class="row justify-content-sm-between justify-content-end align-items-center">
					<div class="col-auto">
						<?php if ($logo = opt('logo')) : ?>
							<a href="/" class="logo">
								<img src="<?= $logo['url'] ?>" alt="logo">
							</a>
						<?php endif; ?>
					</div>
					<div class="col-sm col-auto pad-0-col d-flex justify-content-center align-items-center">
						<button class="hamburger hamburger--spin menu-trigger position-relative" type="button">
							<div class="drop-menu">
								<nav class="drop-nav">
									<?php getMenu('dropdown-menu', '2', '', ''); ?>
								</nav>
							</div>
							<span class="hamburger-box">
								<span class="hamburger-inner"></span>
							</span>
						</button>
						<nav id="MainNav" class="h-100">
							<div id="MobNavBtn">
								<span></span>
								<span></span>
								<span></span>
							</div>
							<?php getMenu('header-menu', '2', '', 'main_menu h-100'); ?>
						</nav>
					</div>
					<div class="col-auto col-header-last pad-0-col">
						<?php if ($contact = opt('contact')) : ?>
							<div class="header-tel-wrap">
								<img src="<?= ICONS ?>header-tel.png" alt="contact-us">
								<?php foreach ($contact as $m => $tel) : if ($tel['tel']) : ?>
									<a href="tel:<?= $tel['tel']; ?>" class="contact-info-footer">
										<?php echo (isset($tel['name']) && $tel['name']) ? $tel['name'] : ''; echo ' '.$tel['tel'];
										echo ($m === 0) ? ' | ': ''; ?>
									</a>
								<?php endif; endforeach; ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ($front) : ?>
		<div class="header-home-transparent">
			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col-xl-11 position-relative">
						<div class="drop-menu">
							<nav class="drop-nav">
								<?php getMenu('dropdown-menu', '2', '', ''); ?>
							</nav>
						</div>
						<div class="row justify-content-between">
							<div class="col-auto">
								<button class="hamburger hamburger--spin menu-trigger" type="button">
									<span class="hamburger-box">
										<span class="hamburger-inner"></span>
									</span>
								</button>
							</div>
							<div class="col-auto col-header-last">
								<div class="header-tel-wrap">
									<img src="<?= ICONS ?>header-tel-white.png" alt="contact-us">
									<?php foreach ($contact as $m => $tel) : if ($tel['tel']) : ?>
										<a href="tel:<?= $tel['tel']; ?>" class="contact-info-footer">
											<?php echo (isset($tel['name']) && $tel['name']) ? $tel['name'] : ''; echo ' '.$tel['tel'];
											echo ($m === 0) ? ' | ': ''; ?>
										</a>
									<?php endif; endforeach; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</header>
<?php
$whatsapp = opt('whatsapp');
$facebook = opt('facebook');
$instagram = opt('instagram');
?>
<div class="fix-wrap">
	<div class="socials-fix">
		<?php if ($whatsapp) : ?>
			<a href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>" target="_blank" class="social-link">
				<img src="<?= ICONS ?>whatsapp.png" alt="whatsapp-call">
			</a>
		<?php endif;
		if ($facebook) : ?>
			<a href="<?= $facebook; ?>" target="_blank" class="social-link">
				<img src="<?= ICONS ?>facebook.png" alt="facebook-call">
			</a>
		<?php endif;
		if ($instagram) : ?>
			<a href="<?= $instagram; ?>" target="_blank" class="social-link">
				<img src="<?= ICONS ?>instagram.png" alt="instagram">
			</a>
		<?php endif; ?>
		<div class="pop-trigger">
			<img src="<?= ICONS ?>pop.png" alt="contact-us">
		</div>
	</div>
</div>
<div class="pop-great">
</div>
<div class="float-form">
	<div class="popup-form">
		<?php if ($popTitle = opt('pop_form_title')) : ?>
			<h2 class="pop-form-title">
				<?= $popTitle; ?>
			</h2>
		<?php endif;
		if ($popSubTitle = opt('pop_form_subtitle')) : ?>
			<h3 class="pop-form-subtitle">
				<?= $popSubTitle; ?>
			</h3>
		<?php endif; ?>
		<?php getForm('194'); ?>
	</div>
</div>
