<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
?>

<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container pt-5">
		<div class="row justify-content-center align-items-stretch mb-5">
			<div class="col-xl-8 col-lg-7 col-12 post-content-col">
				<div class="base-output">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
				<div class="socials-share">
					<span class="share-text"><?= esc_html__('שתף', 'leos'); ?></span>
					<!--	WHATSAPP-->
					<a href="https://api.whatsapp.com/send?text=<?php the_title(); echo $post_link; ?>" class="social-share-link">
						<img src="<?= ICONS ?>whatsapp.png">
					</a>
					<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
					   class="social-share-link">
						<img src="<?= ICONS ?>facebook.png">
					</a>
					<!--	MAIL-->
					<a href="mailto:?subject=&body=<?= $post_link; ?>" target="_blank"
					   class="social-share-link">
						<img src="<?= ICONS ?>share-mail.png">
					</a>
				</div>
			</div>
			<div class="col-xl-4 col-lg-5 col-12 position-relative">
				<?php if (has_post_thumbnail()) : ?>
					<a href="<?= postThumb()?>" download="">
						<img src="<?= postThumb(); ?>" alt="image" class="post-main-img">
					</a>
				<?php endif; ?>
				<div class="form-col-post">
					<?php if ($popTitle = opt('post_form_title')) : ?>
						<h2 class="pop-form-title">
							<?= $popTitle; ?>
						</h2>
					<?php endif;
					if ($popSubTitle = opt('post_form_subtitle')) : ?>
						<h3 class="pop-form-subtitle">
							<?= $popSubTitle; ?>
						</h3>
					<?php endif; ?>
					<?php getForm('195'); ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'category', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => 4,
	'post_type' => 'post',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($samePosts === NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 4,
		'orderby' => 'rand',
		'post_type' => 'post',
		'post__not_in' => array($postId),
	]);
}
if ($samePosts) : ?>
	<section class="posts-block">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col">
					<h2 class="base-title text-center mb-4">
						<?= $fields['same_title'] ? $fields['same_title'] : esc_html__('לכתבות נוספות בתחום', 'leos');?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($samePosts as $post) {
					get_template_part('views/partials/card', 'post',
						[
							'post' => $post,
						]);
				} ?>
			</div>
		</div>
	</section>
<?php
endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
