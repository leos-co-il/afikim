<?php
the_post();
get_header();
$fields = get_fields();
$accordion_info = [
	['title' => 'נתונים טכניים', 'data' => $fields['tech_data']],
	['title' => 'בעלות',  'data' => $fields['ownership']],
	['title' => 'אבזור',  'data' => $fields['arm']],
	['title' => 'תוספות',  'data' => $fields['exstras']],
	['title' => 'אחריות', 'data' => $fields['responsibility']]
];
$post_gallery = $fields['post_gallery'];
$postId = get_the_ID();
$post_terms = wp_get_object_terms( $postId, 'product_brand', ['fields'=>'ids']);
$post_terms = wp_get_object_terms($postId, 'product_cat', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => 5,
	'post_type' => 'product',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'product_cat',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['same_cars']) {
	$samePosts = $fields['same_cars'];
} elseif ($samePosts == NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 5,
		'orderby' => 'rand',
		'post_type' => 'product',
		'post__not_in' => array($postId),
	]);
}
$run = number_format($fields['kilometers']);
$doors = $fields['doors'];
$sits = $fields['seats'];
$transmission = wp_get_object_terms( $postId, 'product_box', ['fields'=>'names']);
$volume = $fields['type_engine'];
$year = $fields['year'];
$engine = wp_get_object_terms( $postId, 'product_engine', ['fields'=>'names']);
$info = [
		['img' => 'doors', 'val' => $doors.' '.'דלתות'],
		['img' => 'seats', 'val' => $sits.' '.'מושבים'],
		['img' => 'type', 'val' => array_shift($transmission).' '.'גיר'],
		['img' => 'engine', 'val' => $volume.' '.'מנוע'],
		['img' => 'year', 'val' => $year.' '.'שנה'],
		['img' => 'gas', 'val' => array_shift($engine)],
];
?>

<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container product-body pt-5">
		<div class="row title-mobile">
			<div class="col-xl-6 col-lg-7 product-info">
				<h2 class="block-title text-right"><?php the_title(); ?></h2>
			</div>
		</div>
		<div class="row justify-content-between">
			<div class="col-xl-6 col-lg-7 product-info">
				<h2 class="block-title text-right title-desktop"><?php the_title(); ?></h2>
				<div class="row">
					<?php foreach ($info as $item) : ?>
						<?php if ($item['val']) : ?>
							<div class="col info-card-item">
								<div class="info-card-icon">
									<img src="<?= ICONS.$item['img'] ?>.png" alt="icon">
								</div>
								<p class="small-card-info">
									<?= $item['val']; ?>
								</p>
							</div>
						<?php endif; ?>
					<?php endforeach; ?>
				</div>
				<?php if ($accordion_info) : ?>
					<div id="accordion-product" class="accordion-product mb-5">
						<?php foreach ($accordion_info as $number => $info_item) : if ($info_item['data']) : ?>
							<div class="card">
								<div class="prod-desc-header" id="heading_<?= $number + 1; ?>">
									<button class="btn button-prod" data-toggle="collapse"
											data-target="#contactInfo<?= $number + 1; ?>"
											aria-expanded="false" aria-controls="collapseOne">
										<span class="product-info-title"><?= $info_item['title']; ?></span>
										<span class="accordion-symbol plus-icon">+</span>
										<span class="accordion-symbol minus-icon">-</span>
									</button>
									<div id="contactInfo<?= $number + 1; ?>" class="collapse"
										 aria-labelledby="heading_<?= $number + 1; ?>" data-parent="#accordion-product">
										<div class="base-output">
											<?= $info_item['data']; ?>
										</div>
									</div>
								</div>
							</div>
						<?php endif; endforeach; ?>
					</div>
				<?php endif; ?>
				<div class="form-col-post mb-4">
					<div class="row justify-content-center mb-4">
						<div class="col-auto">
							<?php if ($base_form_title = opt('prod_form_title')) : ?>
								<h2 class="prod-form-title"><?= $base_form_title; ?></h2>
							<?php endif; ?>
						</div>
						<div class="col-auto">
							<?php if ($base_form_subtitle = opt('prod_form_subtitle')) : ?>
								<h2 class="prod-form-subtitle"><?= $base_form_subtitle; ?></h2>
							<?php endif; ?>
						</div>
					</div>
					<?php getForm('246'); ?>
				</div>
			</div>
			<?php if ($post_gallery || has_post_thumbnail()) : ?>
				<div class="col-xl-6 col-lg-5 col-12 product-gallery-col arrows-slider">
					<div class="arrows-slider post-gallery-block">
						<div class="gallery-slider" dir="rtl">
							<?php if(has_post_thumbnail()): ?>
								<div class="p-1">
									<a class="big-slider-item" style="background-image: url('<?= postThumb(); ?>')"
									   href="<?= postThumb(); ?>" data-lightbox="images"></a>
								</div>
							<?php endif;
							if ($post_gallery) : foreach ($post_gallery as $img): ?>
								<div class="p-1">
									<a class="big-slider-item" style="background-image: url('<?= $img['url']; ?>')"
									   href="<?= $img['url']; ?>" data-lightbox="images">
									</a>
								</div>
							<?php endforeach; endif; ?>
						</div>
						<?php if ($post_gallery) : ?>
							<div class="thumbs-wrap">
								<div class="thumbs" dir="rtl">
									<?php if(has_post_thumbnail()): ?>
										<div class="p-1">
											<a class="thumb-item" style="background-image: url('<?= postThumb(); ?>')"
											   href="<?= postThumb(); ?>" data-lightbox="images-small"></a>
										</div>
									<?php endif; foreach ($post_gallery as $img): ?>
										<div class="p-1">
											<a class="thumb-item" style="background-image: url('<?= $img['url']; ?>')"
											   href="<?= $img['url']; ?>" data-lightbox="images-small">
											</a>
										</div>
									<?php endforeach; ?>
								</div>
								<div class="put-arrows-here"></div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<?php if ($samePosts) : ?>
		<div class="slider-products-block arrows-slider">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12">
						<h2 class="block-title">
							<?php echo $fields['same_cars_title'] ? $fields['same_cars_title'] : 'לרכבים נוספים'; ?>
						</h2>
					</div>
					<div class="col-12">
						<div class="products-slider" dir="rtl">
							<?php foreach ($samePosts as $prod_sl) : ?>
								<div class="py-3">
									<?php get_template_part('views/partials/card', 'product', [
											'product' => $prod_sl,
									]); ?>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php get_footer(); ?>
